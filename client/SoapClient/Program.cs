﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoapClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var Client = new PingServiceReference.PingServiceSoapClient();

            var Result = Client.Ping();

            Console.Write(Result);
        }
    }
}
